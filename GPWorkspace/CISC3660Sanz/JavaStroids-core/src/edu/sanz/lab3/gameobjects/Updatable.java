package edu.sanz.lab3.gameobjects;

public interface Updatable {
	void update(float deltaTime);
}
