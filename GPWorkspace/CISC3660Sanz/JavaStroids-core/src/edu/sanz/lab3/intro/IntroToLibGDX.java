package edu.sanz.lab3.intro;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class IntroToLibGDX extends ApplicationAdapter{

	private SpriteBatch spriteBatch;
	private Sprite bug;
	private Sprite chest;
	int width, height, fps;
	float theX, theY, deltaTime;
	boolean rotation;
	
	@Override
	public void create() {
		// Game Initialization  
		spriteBatch = new SpriteBatch(); 
		theX = 0; theY = 0; rotation = true;
		bug = new Sprite(new Texture("EnemyBug.png"));
		bug.setSize(50, 85);
		bug.setOrigin(0, 0);
		bug.setPosition(theX, theY);
		bug.setRotation(45);
		chest = new Sprite(new Texture("ChestClosed.png"));
		chest.setSize(50, 85);
		chest.setOrigin(chest.getWidth()/2, chest.getHeight()/2);
		chest.setPosition(270, 240);
	}

	@Override
	public void render() {
		// Game Loop
		// find variables (size of window, frames per second & delta time)
		// to move 10 pixels per second. Once a particular edge is touched,
		// revert the direction. Do this forever.
		Gdx.gl.glClearColor(0.7f, 0.7f, 0.2f, 1);
	    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		spriteBatch.begin();
		
		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();
		fps = Gdx.graphics.getFramesPerSecond();
		deltaTime = Gdx.graphics.getDeltaTime();
		
		bug.draw(spriteBatch);
		
		if(rotation && bug.getX() > width*.98 && bug.getY() > height*.98){
			rotation = false;
			bug.setRotation(225);
		}
		if(!rotation && bug.getX() < width*.02 && bug.getY() < height*.02){
			rotation = true;
			bug.setRotation(45);
		}
		if(rotation){
			theX += (10 * deltaTime * fps);
			theY += (10 * deltaTime * fps);
		}
		else {
			theX -= (10 * deltaTime * fps);
			theY -= (10 * deltaTime * fps);
		}
		
		bug.setX(theX % (float)width);
		bug.setY(theY % (float)height);
		
		chest.draw(spriteBatch);
		spriteBatch.end();
	}

	@Override
	public void dispose() {
		spriteBatch.dispose();
		super.dispose();
	}
}
