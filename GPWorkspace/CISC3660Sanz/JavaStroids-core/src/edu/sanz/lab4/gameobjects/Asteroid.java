package edu.sanz.lab4.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import java.util.Random;

import edu.sanz.lab4.game.Constants;

public class Asteroid extends GameObject implements Updatable{

	private float rotationalVel;
	private Vector2 dirAndVel;
	int screenW;
	int screenH;
	
	public Asteroid(Texture tex){
		sprite = new Sprite(tex);
		sprite.setSize(Constants.ASTEROIDS_SIZE, Constants.ASTEROIDS_SIZE); 
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		screenW = Gdx.graphics.getWidth();
		screenH = Gdx.graphics.getHeight();
		Random rand = new Random();
		dirAndVel = new Vector2(rand.nextInt(screenW/10), rand.nextInt(screenH/10));
		setIsDrawable(true);
	}
	
	@Override
	public void update(float deltaTime) {
		if(sprite.getX() > screenW || sprite.getX() < 0)
			dirAndVel.x *= -1;
		if(sprite.getY() > 0 || sprite.getY() < screenH)
			dirAndVel.y *= -1;
		sprite.rotate(getRotVel());
		sprite.translate(dirAndVel.x * deltaTime, -dirAndVel.y * deltaTime);
		
	}
		
	public void setRotVel(float vel){
		rotationalVel = vel;
	}
	public float getRotVel(){
		return rotationalVel;
	}

}
