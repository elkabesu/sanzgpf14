package edu.sanz.lab4.gameobjects;

public interface Updatable {
	void update(float deltaTime);
}
