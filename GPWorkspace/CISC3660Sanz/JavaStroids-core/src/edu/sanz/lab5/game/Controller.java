package edu.sanz.lab5.game;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.math.Vector2;

import edu.sanz.lab5.gameobjects.Asteroid;
import edu.sanz.lab5.gameobjects.GameObject;
import edu.sanz.lab5.gameobjects.Missile;
import edu.sanz.lab5.gameobjects.Ship;

public class Controller {
	
	ArrayList<GameObject> drawableObjects; 
	Ship ship;
	private float screenHeight;
	private Sound thrusterSound;
	private Music backgroundNoise;
	private Sound missileSound;
	private boolean shipCrashed;
	private Sound explosionSound;
	private float explosionX;
	private float explosionY;
	private float missileHitX;
	private float missileHitY;
	private boolean missileHit;
	
	public Controller(){
		drawableObjects = new ArrayList<GameObject>(); 
		initSound();
		initShip();
		initAsteroids(10);
		screenHeight = Gdx.graphics.getHeight();
	}
	
	private void initShip(){
		int w = Constants.SHIP_WIDTH; 
		int h = Constants.SHIP_HEIGHT; 
		Pixmap pmap = new Pixmap(w, h, Format.RGBA8888); // TODO: Check Image Format
		pmap.setColor(1, 1, 1, 1);
		pmap.drawLine(0, h, w/2, 0);
		pmap.drawLine(w, h, w/2, 0);
		pmap.drawLine(1, h-1, w, h-1);
		ship = new Ship(new Texture(pmap), 100, 100);
		drawableObjects.add(ship);
	}
	
	private void initAsteroids(int num){
		Random rand = new Random();
		for(int i = 0; i<num; i++){
			Asteroid asteroid = new Asteroid(new Texture("Asteroid_tex.png"));
			asteroid.sprite.setPosition(rand.nextInt(Gdx.graphics.getWidth()), rand.nextInt(Gdx.graphics.getHeight()));
			asteroid.sprite.setOrigin(asteroid.sprite.getWidth() / 2, asteroid.sprite.getHeight() / 2);
			asteroid.setRotVel(rand.nextFloat()*8-4);
			drawableObjects.add(asteroid);
		}
	}
	
	private void initMissile(){
		int w = Constants.SHIP_WIDTH/2;
		int h = Constants.SHIP_HEIGHT/2;
		Pixmap pmap = new Pixmap(w, h, Format.RGB565);
		pmap.setColor(1, 1, 1, 1);
		pmap.drawLine(w/2, 0, w/2, h);
		drawableObjects.add(new Missile(new Texture(pmap), ship.getDirection(), ship.getPosition()));
	}
	
	public void update(){
		processKeyboardInput();
		processMouseInput();
		float deltaT = Gdx.graphics.getDeltaTime();
		setMissileHit(false);
		for(int i = 0; i < drawableObjects.size(); i++){
			GameObject gObj = drawableObjects.get(i);
			if(gObj instanceof Asteroid){
				if(ship.sprite.getBoundingRectangle().overlaps(((Asteroid) gObj).sprite.getBoundingRectangle()) & !isShipCrashed()){
					setShipCrashed(true);
					explosionSound.play();
					thrusterSound.stop();
					setExplosionX(ship.sprite.getX());
					setExplosionY(ship.sprite.getY());
					drawableObjects.remove(((Asteroid) gObj));
					drawableObjects.remove(((Ship) ship));
					initShip();
					
				}
				((Asteroid) gObj).update(deltaT); 
			}
			if(gObj instanceof Missile){
				for(int j = drawableObjects.size()-1; j >= 0; j--){                     // Check for instances of skills hitting the creep
                    if(drawableObjects.get(j) instanceof Asteroid){
                            GameObject gObjA = drawableObjects.get(j);
                            if(((Asteroid) gObjA).sprite.getBoundingRectangle().overlaps(((Missile) gObj).sprite.getBoundingRectangle())){
                            	setMissileHitX(((Asteroid) gObjA).sprite.getX());
                            	setMissileHitY(((Asteroid) gObjA).sprite.getY());
                            	drawableObjects.remove(((Missile) gObj));
                            	drawableObjects.remove(((Asteroid) gObjA));
                            	setMissileHit(true);
                            }
					}
				}
				((Missile) gObj).update(deltaT); 
			}
		}
		// Update ship
		if(!isShipCrashed())
			ship.update(deltaT);
		else drawableObjects.remove(ship);
	}
	
	private void initSound(){
		thrusterSound = Gdx.audio.newSound(Gdx.files.internal("rocket.mp3"));
		setMissileSound(Gdx.audio.newSound(Gdx.files.internal("missile.mp3")));
		backgroundNoise = Gdx.audio.newMusic(Gdx.files.internal("background_music.mp3"));
		explosionSound = Gdx.audio.newSound(Gdx.files.internal("explosion.mp3"));
		backgroundNoise.setLooping(true);
		backgroundNoise.play();
		backgroundNoise.setVolume(0.4f);
	}
	
	private void processKeyboardInput(){
		if (Gdx.app.getType() != ApplicationType.Desktop) return; // Just in case :)
		if (Gdx.input.isKeyPressed(Keys.UP)) ship.moveForward(Gdx.graphics.getDeltaTime());
		if (Gdx.input.isKeyPressed(Keys.UP)) thrusterSound.play(0.4f);
		if (Gdx.input.isKeyPressed(Keys.SPACE)) initMissile();
		if (Gdx.input.isKeyPressed(Keys.SPACE)) missileSound.play(0.2f);
	}
	
	private void processMouseInput(){
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
			ship.face(new Vector2(Gdx.input.getX()-ship.sprite.getX(),
					-(screenHeight - Gdx.input.getY() - ship.sprite.getY())));
		}
	}
	
	public void dispose(){
		if(thrusterSound != null)
			thrusterSound.dispose();
		if(backgroundNoise != null)
			backgroundNoise.dispose();
	}
	
	public ArrayList<GameObject> getDrawableObjects(){
		return drawableObjects;
	}

	public Sound getThrusterSound() {
		return thrusterSound;
	}

	public void setThrusterSound(Sound thrusterSound) {
		this.thrusterSound = thrusterSound;
	}

	public Music getBackgroundNoise() {
		return backgroundNoise;
	}

	public void setBackgroundNoise(Music backgroundNoise) {
		this.backgroundNoise = backgroundNoise;
	}

	public Sound getMissileSound() {
		return missileSound;
	}

	public void setMissileSound(Sound missileSound) {
		this.missileSound = missileSound;
	}

	public boolean isShipCrashed() {
		return shipCrashed;
	}

	public void setShipCrashed(boolean shipCrashed) {
		this.shipCrashed = shipCrashed;
	}

	public Sound getExplosionSound() {
		return explosionSound;
	}

	public void setExplosionSound(Sound explosionSound) {
		this.explosionSound = explosionSound;
	}

	public float getExplosionX() {
		return explosionX;
	}

	public void setExplosionX(float explosionX) {
		this.explosionX = explosionX;
	}

	public float getExplosionY() {
		return explosionY;
	}

	public void setExplosionY(float explosionY) {
		this.explosionY = explosionY;
	}

	public float getMissileHitX() {
		return missileHitX;
	}

	public void setMissileHitX(float missileHitX) {
		this.missileHitX = missileHitX;
	}

	public float getMissileHitY() {
		return missileHitY;
	}

	public void setMissileHitY(float missileHitY) {
		this.missileHitY = missileHitY;
	}

	public boolean isMissileHit() {
		return missileHit;
	}

	public void setMissileHit(boolean missileHit) {
		this.missileHit = missileHit;
	}
}
