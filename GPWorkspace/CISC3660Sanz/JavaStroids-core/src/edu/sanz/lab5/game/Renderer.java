package edu.sanz.lab5.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import edu.sanz.lab5.gameobjects.GameObject;

public class Renderer {
	
	private SpriteBatch spriteBatch;
	private Controller control;
	BitmapFont font;
	private Texture bg1;
	private Texture bg2;
	private int bg2XPos;
	private int bg1XPos;
	Animation explosionAnim;
	Texture explosionSheet;
	TextureRegion [] explosionFrames;
	TextureRegion currentFrameExplosion;
	float shipExplosionStateTime;
	private TextureRegion [][] tmp;
	private int index;
	private float asteroidExplosionStateTime;
	
	public Renderer(Controller c){
		control = c;
		spriteBatch = new SpriteBatch(); 
		font = new BitmapFont();
		setBg1(new Texture("background_image.jpg"));
		setBg2(new Texture("background_image.jpg"));
		bg1XPos = 0;
		bg2XPos = getBg1().getWidth() / 2;
		explosionSheet = new Texture(Gdx.files.internal("explosion17.png"));
		tmp = TextureRegion.split(explosionSheet, explosionSheet.getWidth()/5, explosionSheet.getHeight()/5);
		explosionFrames = new TextureRegion[25];
		for(int i = 0; i <5; i++){
			for(int j = 0; j < 5; j++){
				explosionFrames[index++] = tmp[i][j];
			}
		}
		explosionAnim = new Animation(0.04f, explosionFrames);
		shipExplosionStateTime = 0f;
		asteroidExplosionStateTime = 0f;
	}
	
	public void render(){
		spriteBatch.begin();
		renderBackground();
		for(GameObject gObj : control.getDrawableObjects()){
			gObj.sprite.draw(spriteBatch);
		}
		if(control.isShipCrashed() && !explosionAnim.isAnimationFinished(shipExplosionStateTime)){
			shipExplosionStateTime += Gdx.graphics.getDeltaTime();
			currentFrameExplosion = explosionAnim.getKeyFrame(shipExplosionStateTime, false);
			spriteBatch.draw(currentFrameExplosion, control.getExplosionX() - Constants.SHIP_WIDTH, control.getExplosionY() - Constants.SHIP_HEIGHT);
		}
		if(control.isMissileHit() && !explosionAnim.isAnimationFinished(shipExplosionStateTime)){
			asteroidExplosionStateTime += Gdx.graphics.getDeltaTime();
			currentFrameExplosion = explosionAnim.getKeyFrame(asteroidExplosionStateTime, false);
			spriteBatch.draw(currentFrameExplosion, control.getMissileHitX() - Constants.ASTEROIDS_SIZE, control.getMissileHitY() - Constants.ASTEROIDS_SIZE);
		}
	
		spriteBatch.end();
	}

	public void renderBackground() {
		spriteBatch.draw(getBg1(), bg1XPos, 0);
		spriteBatch.draw(getBg2(), bg2XPos, 0);
		
		if(bg2XPos == 0){
			bg1XPos = 0;
			bg2XPos = getBg1().getWidth() / 2;
		}
		
		bg1XPos -= .3;
		bg2XPos -= .3;
		
		
	}

	public Texture getBg1() {
		return bg1;
	}

	public void setBg1(Texture bg1) {
		this.bg1 = bg1;
	}

	public Texture getBg2() {
		return bg2;
	}

	public void setBg2(Texture bg2) {
		this.bg2 = bg2;
	}

}
