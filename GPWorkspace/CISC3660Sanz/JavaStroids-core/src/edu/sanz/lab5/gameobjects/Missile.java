package edu.sanz.lab5.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;


public class Missile extends GameObject implements Updatable {

	private Vector2 dirAndVel;
	Vector2 pos;
	private final float VELOCITY = 100;
	int screenW;
	int screenH;
	
	public Missile(Texture tex, Vector2 direction, Vector2 position){
		sprite = new Sprite(tex);
		sprite.setPosition(position.x, position.y);
		dirAndVel = direction.scl(VELOCITY, VELOCITY);
		pos = position;
		screenW = Gdx.graphics.getWidth();
		screenH = Gdx.graphics.getHeight();
		setIsDrawable(true);
	}
	
	@Override
	public void update(float deltaTime) {
		pos.x += dirAndVel.x * deltaTime + 1;
		pos.y -= dirAndVel.y * deltaTime + 1;
		sprite.setPosition(pos.x, pos.y);
		//if(sprite.getX() > screenW || sprite.getX() < 0)
			
		
	}

}
