package edu.sanz.lab5.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import edu.sanz.lab5.game.Constants;

public class Ship extends GameObject implements Updatable{
	
	private Vector2 direction;
	private Vector2 targetDirection;
	private Vector2 velocity;
	private Vector2 temp;
	private final float MIN_VELOCITY = 20;
	int i;
	
	public Ship(Texture texture, int x, int y) {
		sprite = new Sprite(texture);
		sprite.setOrigin(texture.getWidth()/2, texture.getHeight()/2);
		sprite.setPosition(x, y);
		direction = new Vector2(0, -1);
		targetDirection = new Vector2(0, -1);
		velocity = new Vector2(0, MIN_VELOCITY);
		temp = new Vector2(0,0);
		setIsDrawable(true);
	}
	
	public void face(Vector2 targetPos){
		targetDirection = targetPos;
	}
	
	@Override
	public void update(float deltaTime) {
		temp.set(targetDirection);
		temp.sub(direction);
		targetDirection.nor();
		if(targetDirection.x >= 0f)
			sprite.setRotation((float)-Math.toDegrees(Math.acos(direction.dot(targetDirection))));
		else
			sprite.setRotation((float)Math.toDegrees(Math.acos(direction.dot(targetDirection))));
		sprite.translate(velocity.x * deltaTime, velocity.y * deltaTime);
		if(velocity.x > MIN_VELOCITY || velocity.y > MIN_VELOCITY) velocity.scl(1 - deltaTime);
		if(sprite.getX() >= Gdx.graphics.getWidth()){
			sprite.setX(0f);
			velocity.set(10,-10);
		}
		if(sprite.getX() <= 0f){
			sprite.setX(Gdx.graphics.getWidth());
			velocity.set(10,-10);
		}			
		if(sprite.getY() >= Gdx.graphics.getHeight()){
			sprite.setY(0f);
			velocity.set(10,-10);
		}
		if(sprite.getY() <= 0f){
			sprite.setY(Gdx.graphics.getHeight());
			velocity.set(10,-10);
		}		
	}

	public void moveForward(float deltaTime) {
//		Student, your code goes here	
		velocity.x += temp.x * 10 * deltaTime;
		velocity.y -= temp.y * 10 * deltaTime;
	}
	
	public Vector2 getDirection(){
		return temp.nor();
	}
	
	public Vector2 getPosition(){
		return new Vector2(sprite.getX() + Constants.SHIP_WIDTH/2, sprite.getY() + Constants.SHIP_HEIGHT/2);
	}
	
	
}
