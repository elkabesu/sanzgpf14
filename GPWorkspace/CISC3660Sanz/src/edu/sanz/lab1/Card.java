/**
 * 
 */
package edu.sanz.lab1;

/**
 * @author Derek Sanz 
 *
 */
public class Card {
	private Suit suit;
	private Rank rank;
	
	public Card(Suit suit, Rank rank){
		this.suit = suit;
		this.rank = rank;
	}
	
	public Suit getSuit(){
		return suit;
	}
	
	public void setSuit(Suit suit){
		this.suit = suit;
	}
	
	public Rank getRank(){
		return rank;
	}
	
	public void setRank(Rank rank){
		this.rank = rank;
	}
	
	public static boolean compare(Card c1, Card c2){
		Suit c1Suit, c2Suit;
		Rank c1Rank, c2Rank;
		c1Suit = c1.suit;
		c2Suit = c2.suit;
		c1Rank = c1.rank;
		c2Rank = c2.rank;
		
		if(c1Suit == c2Suit && c1Rank == c2Rank){
			return true;
		}
		else
			return false;
		
	}
	
	public static boolean compareRank(Card c1, Card c2){
		Rank c1Rank, c2Rank;
		c1Rank = c1.rank;
		c2Rank = c2.rank;
		
		if(c1Rank == c2Rank){
			return true;
		}
		else
			return false;
		
	}
	
	public static boolean compareSuit(Card c1, Card c2){
		Suit c1Suit, c2Suit;
		c1Suit = c1.suit;
		c2Suit = c2.suit;
		
		if(c1Suit == c2Suit){
			return true;
		}
		else
			return false;
		
	}
	
	public String toString(){
		String currentSuit, currentRank, s;
		currentSuit = this.getSuit().toString();
		currentRank = this.getRank().toString();
		s = currentSuit + " " + currentRank;
		return s;
	}
}


