/**
 * 
 */
package edu.sanz.lab1;

import java.util.*;

/**
 * @author Derek Sanz
 *
 */
public class Deck {

	private ArrayList<Card> deck = new ArrayList<Card>();
	
	public Deck(){
		
	}
	
	public void initFullDeck(){
		deck.removeAll(deck);
		for(Rank r: Rank.values()){
			for(Suit s: Suit.values()){
				deck.add(new Card(s,r));
			}
		}
	}
	
	public void initEmptyDeck(){
		deck.removeAll(deck);
	}
	
	public List<Card> getDeck(){
		return Collections.unmodifiableList(deck);
	}
	
	public Card removeCardDeck(int index){
		return deck.remove(index);
	}
	
	public void addOneCard(Card c){
		deck.add(c);
	}
	
	//Remove one card of some rank and suit
	public void removeAParticularCard(Card c){
		for(int i = deck.size()-1; i >= 0; i--){
			if(deck.get(i).getRank() == c.getRank() && deck.get(i).getSuit() == c.getSuit()){
				deck.remove(i);
				break;
			}
		}
	}
	
	public void removeAllCardsOfRank(Rank r){
		for(int i = deck.size()-1; i >= 0; i--){
			if(deck.get(i).getRank() == r){
				deck.remove(i);
			}
		}
	}
	
	@Override
	public String toString(){
		String s = "";
		String currentSuit;
		String currentRank;
		for(Card c: deck){
			currentSuit = c.getSuit().toString();
			currentRank = c.getRank().toString();
			s = s + currentSuit + " " + currentRank + "\n";
		}
		return s;
	}
	
	public ArrayList<Card> getOrderedCards(){
		ArrayList<Card> temp = new ArrayList<Card>();
		Card tempCard;
		for(Rank r: Rank.values()){
			for(int i = 0; i < deck.size(); i++){
				if(deck.get(i).getRank() == r){
					tempCard = new Card(deck.get(i).getSuit(), deck.get(i).getRank());
					temp.add(tempCard);
				}
			}
		}
		return temp;
	}
	
	public int getNumberOfCardsRemaining(){
		return deck.size();
	}
	
	public Card dealCard(){
		Card card;
		card = deck.get(0);
		deck.remove(0);
		return card;
	}
	
	public void shuffle(){
		Card randomCard;
		double random;
		int rand,i,dSize;
		dSize = deck.size();
		for(i = 0; i < dSize; i++){
			random = Math.random() * dSize;
			rand = (int)random;
			randomCard = deck.get(rand);
			deck.remove(rand);
			deck.add(randomCard);
		}
	}
	
	public void shuffleSevenTimes(){
		for(int i = 0; i <7; i++){
			shuffle();
		}
	}
}
