package edu.sanz.lab1;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		lab1();
	}
		
	public static void lab1(){
		int i,numCards, countOne = 0, countTwo = 0;
		ArrayList<Card> arrayOne = new ArrayList<Card>();
		ArrayList<Card> arrayTwo = new ArrayList<Card>();
		Deck deck = new Deck();
		deck.initFullDeck();
		deck.shuffle();
		deck.shuffle();
		
		
		numCards = deck.getNumberOfCardsRemaining();
		for(i = 0; i < numCards; i = i + 2){
			arrayOne.add(deck.dealCard());
			arrayTwo.add(deck.dealCard());
		}	
		
		for(i = 0; i < arrayOne.size(); i++){
			countOne += arrayOne.get(i).getRank().ordinal();
			countTwo += arrayTwo.get(i).getRank().ordinal();
		}
		
		System.out.println("Array one's sum is " + countOne);
		System.out.println("Array two's sum is " + countTwo);
		if(countOne > countTwo)
			System.out.println("Array one has a greater sum than array two");
		else if(countTwo > countOne)
			System.out.println("Array two has a greater sum than array one");
		else
			System.out.println("Array one and array two have the same sum");
	}
	
	public static void testLab1(){
		Deck deck = new Deck();
		System.out.println(deck.toString());
	}
		

}
