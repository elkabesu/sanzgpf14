/**
 * 
 */
package edu.sanz.lab1;

/**
 * @author Derek Sanz
 *
 */
public enum Rank {
	ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING;
}
