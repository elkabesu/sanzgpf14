/**
 * 
 */
package edu.sanz.lab1;

/**
 * @author Derek Sanz
 *
 */
public enum Suit {
	CLUBS, DIAMONDS, HEARTS, SPADES
}
