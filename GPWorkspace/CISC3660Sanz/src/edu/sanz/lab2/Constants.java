/**
 * 
 */
package edu.sanz.lab2;

/**
 * @author Derek Sanz
 *
 */
public class Constants {
	final static String WAIT_MSG = "Wait for other players to finish their turn";
	final static String WRONG_TURN_MSG = "IT IS NOT YOUR TURN!";
	final static String WON = "You won!";
	final static String LOST = "You lost! :(";
	final static String BLACKJACK = "Blackjack!";
	final static String TOTAL = " TOTAL: ";
	final static String DEALERS_TOTAL = "The Dealer's total: ";
	final static String PLAYER1 = " It is now Player 1's turn. "; 
	final static String PLAYER2 = " It is now Player 2's turn. ";
	final static String PLAYER3 = " It is now Player 3's turn. ";
	public final static int MAX_NUM_PLAYERS = 4;
}
