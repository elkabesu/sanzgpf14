/**
 * 
 */
package edu.sanz.lab2;
import java.util.ArrayList;

/**
 * @author Derek Sanz
 *
 */
public class Controller {
	MessagePanel msg;
	boolean playing;
	State state;
	
	public Controller(){}
	
	public void init(){
		int i1Sum, i2Sum, i3Sum;
		state = new State();
		
		i1Sum = rankOfCard(1,0) + rankOfCard(1,1) + 2;
		i2Sum = rankOfCard(2,0) + rankOfCard(2,1) + 2;
		i3Sum = rankOfCard(3,0) + rankOfCard(3,1) + 2;
		
		//send message to player 1
		msg.outputMessage(1, "It is your turn. Your hand: " + state.getPlayer(1).getHandString() + Constants.TOTAL + i1Sum);
		msg.outputMessage(2, state.getPlayer(2).getHandString() + Constants.TOTAL + i2Sum + " " + Constants.WAIT_MSG);
		msg.outputMessage(3, state.getPlayer(3).getHandString() + Constants.TOTAL + i3Sum + " " + Constants.WAIT_MSG);
		msg.outputMessage(4, Constants.DEALERS_TOTAL + state.getPlayer(0).getHandString());
	}
	
	public void clientRequest(char c){
		if(playing){
			changeState(c);
			return;
		}
		init();
		playing = true;
	}
	
	public void setMessenger(MessagePanel message){
		msg = message;
		msg.showMenu();
	}
	
	private void showErrorMsgPlayerKey(char c){
		int player = -1;
		if(c == 'q' || c == 'w'){
			player = 1;
		}
		else if(c == 'a' || c == 's'){
			player = 2;
		}
		else if(c == 'z' || c == 'x'){
			player = 3;
		}
		msg.outputMessage(player, Constants.WRONG_TURN_MSG);
	}
	
	private void changeState(char c){
		String keys = "qwaszxQWASZX";
		
		int turn = state.getTurn();
		int player1Sum, player2Sum, player3Sum, i;
		int dealerSum = 0;
		
		boolean lost1 = false, 
				lost2 = false, 
				lost3 = false;
		
		if(lost1 && lost2 && lost3)
			gameWon(4);
		
		for(i = 0; i < cardsInHand(0); i++){
			if(rankOfCard(0,i) < 10)
				dealerSum += rankOfCard(0,i) + 1;
			else dealerSum += 10;
		}
		
		msg.outputMessage(4, Constants.DEALERS_TOTAL + dealerSum);
		
		if(dealerSum == 21)
			gameWon(4);
			
		if(keys.indexOf(c) == -1){
			return;
		}
		else if(turn % 3 == 1){
			while(lost1 != true){
				player1Sum = 0;
				if(c == 'q'){
					dealPlayerCard(1);
					for(i = 0; i < cardsInHand(1); i++){
						if(rankOfCard(1,i) < 10){
							if(rankOfCard(1,i) == 0 && player1Sum == 10)
								player1Sum += 10;
							player1Sum += rankOfCard(1,i) + 1;
						}
						else player1Sum += 10;
					}
					msg.outputMessage(1, state.getPlayer(1).getHandString() + Constants.TOTAL + player1Sum + Constants.PLAYER2);
					
	 				if(player1Sum == 21)
						gameWon(1);
	 				
					else if(player1Sum > 21){
						playerLost(1);
						lost1 = true;
						state.nextTurn();
						return;
					}
					state.nextTurn();
				}
				else if(c == 'w'){
					msg.outputMessage(1, state.getPlayer(1).getHandString() + Constants.PLAYER2);
					state.nextTurn();
				}
				else showErrorMsgPlayerKey(c);
				return;
			}
			state.nextTurn();
		}
				
		else if(turn % 3 == 2){
			while(!lost2){
				player2Sum = 0;
				if(c == 'a'){
					dealPlayerCard(2);
					for(i = 0; i < cardsInHand(2); i++)
						if(rankOfCard(2,i) < 10){
							if(rankOfCard(2,i) == 0 && player2Sum == 10)
								player2Sum += 10;
							player2Sum += rankOfCard(2,i) + 1;
						}
						else player2Sum += 10;
					msg.outputMessage(2, state.getPlayer(2).getHandString() + Constants.TOTAL + player2Sum 	+ Constants.PLAYER3);
					
					if(player2Sum == 21)
						gameWon(2);
					
					else if(player2Sum > 21){
						playerLost(2);
						lost2 = true;
						state.nextTurn();
						return;
					}
					state.nextTurn();
				}
				else if(c == 's'){
					msg.outputMessage(2, state.getPlayer(2).getHandString() + Constants.PLAYER3);
					state.nextTurn();
				}
				else showErrorMsgPlayerKey(c);
				return;
			}
			state.nextTurn();
		}
		
		else if(turn  % 3 == 0){
			while(!lost3){
				player3Sum = 0;
				if(c == 'z'){
					dealPlayerCard(3);
					for(i = 0; i < cardsInHand(3); i++)
						if(rankOfCard(3,i) < 10){
							if(rankOfCard(3,i) == 0 && player3Sum == 10)
								player3Sum += 10;
							player3Sum += rankOfCard(3,i) + 1;
						}
						else player3Sum += 10;
					msg.outputMessage(3, state.getPlayer(3).getHandString() + Constants.TOTAL + player3Sum + Constants.PLAYER1);
					
					if(player3Sum == 21)
						gameWon(3);
					
					else if(player3Sum > 21){
						playerLost(3);
						state.nextTurn();
						return;
					}
					state.nextTurn();
				}
				else if(c == 'x'){
					msg.outputMessage(3, state.getPlayer(3).getHandString() + Constants.PLAYER1);
					dealPlayerCard(3);
					state.nextTurn();
				}
				else showErrorMsgPlayerKey(c);
				return;
			}
			return;
		}
	}
	
	public void gameWon(int player){
		msg.outputMessage(player, Constants.WON);
		try{Thread.sleep(1000);}
		catch(InterruptedException ex) {Thread.currentThread().interrupt();}
		playing = false;
	}
	
	public int cardsInHand(int player){
		return state.getPlayer(player).getHand().size();
	}
	
	public int rankOfCard(int player, int index){
		return state.getPlayer(player).getHand().get(index).getRank().ordinal();
	}
	
	public void dealPlayerCard(int player){
		state.getPlayer(player).receiveCard(state.getDeck().dealCard());
	}
	
	public void playerLost(int player){
		try{Thread.sleep(1000);}
		catch(InterruptedException ex) {Thread.currentThread().interrupt();}
		msg.outputMessage(player, Constants.LOST + " " + Constants.WAIT_MSG);
	}
}
