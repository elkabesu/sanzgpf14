/**
 * 
 */
package edu.sanz.lab2;

import edu.sanz.lab1.Card;
import java.util.ArrayList;

/**
 * @author Derek Sanz
 *
 */
public class Player {
	private ArrayList<Card> hand;
	
	public Player(){
		hand = new ArrayList<Card>();
	}
	
	public void receiveCard(Card c){
		hand.add(c);
	}
	
	public ArrayList<Card> getHand(){
		return hand;
	}
	
	public String getHandString(){
		String handStr = "";
		for(Card c: hand){
			handStr += c.getRank() + " ";
		}
		return handStr;
	}
}
