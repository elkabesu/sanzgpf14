/**
 * 
 */
package edu.sanz.lab2;

import java.awt.event.*;
import javax.swing.JFrame;


/**
 * @author Derek Sanz
 *
 */
public class Server extends JFrame implements KeyListener {
	MessagePanel message;
	Controller controller;
	public Server(Controller cont){
		controller = cont;
		message = new MessagePanel();
		cont.setMessenger(message);
		add(message);
		setTitle("Game Server");
		setSize(810,810);
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		addKeyListener(this);
		requestFocus();
	}
	
	@Override
	public void keyTyped(KeyEvent e){
		char c = e.getKeyChar();
		controller.clientRequest(c);
	}
	
	@Override
	public void keyPressed(KeyEvent e){}
	
	@Override
	public void keyReleased(KeyEvent e){}
}
