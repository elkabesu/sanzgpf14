/**
 * 
 */
package edu.sanz.lab2;

import edu.sanz.lab1.Deck;

/**
 * @author Derek Sanz
 *
 */
public class State {
	private byte turn;
	private Player [] players;	//player[0] is the dealer
	private Deck deck;
	public State(){
		init();
	}
	
	public void init(){
		int i;
		turn = 1;
		deck = new Deck();
		deck.initFullDeck();
		deck.shuffleSevenTimes();
		players = new Player[Constants.MAX_NUM_PLAYERS + 1];
		players[0] = new Player();
		players[0].receiveCard(deck.dealCard());
		
		for(i = 1; i < Constants.MAX_NUM_PLAYERS; i++){
			players[i] = new Player();
			players[i].receiveCard(deck.dealCard());
			players[i].receiveCard(deck.dealCard());
		}
	}
	
	public Player getPlayer(int playerNum){
		return players[playerNum];
	}
	
	public Player getDealer(){
		return players[0];
	}
	
	public Deck getDeck(){
		return deck;
	}
	
	public byte getTurn(){
		return turn;
	}
	
	public void nextTurn(){
		turn++;
	}
}
